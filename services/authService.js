const moment = require('moment');
require('dotenv').config();
const jwt = require('jsonwebtoken');

//import services
const redisService = require('./redisService');

const {
    auth: { username, password},
    jwt :{ secret, expiresIn, issuer, audience, algorithm }
} = require('../config/config');
const redisConstant = require('../constant/redis');

const jwtOptions = {
    expiresIn, issuer, audience, algorithm,
}

class AuthService {
    async login(body) {
        if(body.username == username && body.password == password){

            const lastLogin = moment();
            let token;

            //get token from redis
            const getToken = await redisService.fetch(redisConstant.AUTH);
            if(getToken) token = getToken;

            if(!token){

                // generate jwt token
                token = jwt.sign({
                    username,
                    lastLogin,
                }, secret, jwtOptions);

                // save token to redis
                redisService.store(redisConstant.AUTH, token);
            }

            return {
                username,
                lastLogin,
                token,
            };

        }else{
            throw { status : false, message : 'invalid credential', code: 401 };
        }
    }
}

module.exports = new AuthService();