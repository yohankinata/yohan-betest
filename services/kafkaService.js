const { Kafka } = require('kafkajs')

// define services
const redisService = require('../services/redisService');

// define constant
const redisConstant = require('../constant/redis');
const kafkaConstant = require('../constant/kafka');

const kafka = new Kafka({
    clientId: 'my-app',
    brokers: ['localhost:9092', 'localhost:9092']
});

class KafkaService {
    async producer(res){
        const producer = kafka.producer();

        await producer.connect();
        await producer.send({
            topic: kafkaConstant.USER_TOPIC,
            //convert value to a JSON string and send it
            messages: [{ value: JSON.stringify(res) }],
        });
        //console.log('Message sent successfully', res);
    }

    async consumer() {
        const consumer = kafka.consumer({ groupId: 'kafka' });

        await consumer.connect();
        await consumer.subscribe({topic: kafkaConstant.USER_TOPIC, fromBeginning: true })
        await consumer.run({
            eachMessage: async ({  message }) => {
                //console.log("****************** Arrived in Consumer ******************")
                const jsonObj = JSON.parse(message.value.toString());
                //clear user list from redis
                if(jsonObj) redisService.destroy(redisConstant.USERS);
            },
        });
    }
}

module.exports = new KafkaService();