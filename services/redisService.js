const redis = require('redis')

const { redis: { host, port, password } } = require('../config/config');
const redisConfig = password == '' || !password ? { host, port } : { host, port, password }
const redisClient = redis.createClient(redisConfig);

redisClient.on('connect', () => {
    console.log('Connected to redis');
})

redisClient.on('error', (err) => {
    console.log(err.message);
})

redisClient.on('ready', () => {
    console.log('Redis is ready');
})

redisClient.on('end', () => {
    console.log('Redis connection ended');
})

class RedisService {
    async connect(){
        return await redisClient.connect();
    }

    async hStore(key, index,value) {
        return redisClient.hSet(key, index,value)
    }

    async hFetch(key,index){
        return redisClient.hGet(key, index)
    }

    async store(key, value) {
        return redisClient.set(key, value)
    }

    asynctimeout(key, time) {
        return redisClient.expire(key, time)
    }

    async fetch(key) {
        return redisClient.get(key);
    }

    destroy(key) {
        return redisClient.del(key)
    }
}

module.exports = new RedisService();