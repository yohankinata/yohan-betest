const moment = require('moment');

// define services
const redisService = require('../services/redisService');
const kafkaService = require('../services/kafkaService');

// defince model
const userEntity = require('../models/entity/usersEntity');

// define constant
const redisConstant = require('../constant/redis');

class UserService {
    async create(body) {
        const user = new userEntity({
            userName:body.userName,
            accountNumber:body.accountNumber,
            emailAddress:body.emailAddress,
            identityNumber:body.identityNumber,
            createdAt:moment(),
            updatedAt:moment(),
        });

        const checkAccountNumber = await this.getByParams('accountNumber',user.accountNumber);
        if(checkAccountNumber) throw { status : false, message: 'accountNumber already used!', code : 400 };

        const checkIdentityNumber = await this.getByParams('identityNumber',user.identityNumber);
        if(checkIdentityNumber) throw { status : false, message: 'identityNumber already used!', code : 400 };

        await user.save();

        //sent to kafka
        await kafkaService.producer(user);

        return user;
    }

    async getAll(){

        let users;

        // fetch redis user list
        users = await redisService.fetch(redisConstant.USERS);
        users = JSON.parse(users);
        if(users) return users;

        users = await userEntity.find({
            deletedAt: {$eq:null},
        });

        // set users to redis
        await redisService.store(redisConstant.USERS,JSON.stringify(users));

        return users;
    }

    async getByParams(getBy = '',value = ''){

        if(!getBy || !value) return false;

        let user;

        // fetch redis user
        user = await redisService.hFetch(redisConstant.USER,`${getBy}_${value}`);
        user = JSON.parse(user);
        if(user) return user;

        const query = {};
        query[getBy] = value;
        query['deletedAt'] = {$eq:null};

        user = await userEntity.findOne(query);

        if(user){
            // set redis user
            await redisService.hStore(redisConstant.USER,`${getBy}_${value}`,JSON.stringify(user));
        }

        return user;
    }

    async update(id,body) {
        const user = await userEntity.findById(id);
        if(!user) throw { status : false, message: 'user not found!', code: 400 };

        Object.assign(user, {
            userName:body.userName,
            accountNumber:body.accountNumber,
            emailAddress:body.emailAddress,
            identityNumber:body.identityNumber,
            updatedAt:moment(),
        })

        await user.save();

        //sent to kafka
        await kafkaService.producer(user);
        // destroy redis user
        await redisService.destroy(redisConstant.USER);

        return user;
    }

    async delete(id) {
        const user = await userEntity.findById(id);

        if(!user) throw { status : false, message: 'user not found!', code: 400 };
        if(user.deletedAt) throw { status : false, message: 'user already deleted!', code: 400 };

        Object.assign(user, {
            deletedAt:moment(),
        })

        await user.save();

        //sent to kafka
        await kafkaService.producer(user);

        return user;
    }
}

module.exports = new UserService();