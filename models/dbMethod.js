const config = require('../config/config');
const mongoose = require('./dbClient');

const {
  connectionString, autoIndex,
} = config.mongoDB

const options = {
  autoIndex,
  maxPoolSize: 2,
  // useNewUrlParser: true, // <-- no longer necessary
  // useCreateIndex: true, // <-- no longer necessary
  // useUnifiedTopology: true, // <-- no longer necessary
  connectTimeoutMS: 30000, // Give up initial connection after 30 seconds
  socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
}

class DBConnection {
  connect() {
    return mongoose.connect(connectionString, options)
  }

  disconnect() {
    return mongoose.connection.close()
  }
}

module.exports = new DBConnection()
