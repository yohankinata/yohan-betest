const mongoose = require('mongoose');

const { Schema } = mongoose
const userSchema = new Schema({
    id: {
        type: Number,
    },
    userName: {
        type: String,
    },
    accountNumber: {
        type: Number,
    },
    emailAddress: {
        type: String,
    },
    identityNumber: {
        type: Number,
    },
    createdAt: {
        type: Date,
    },
    updatedAt: {
        type: Date,
    },
    deletedAt: {
        type: Date,
    }
}, { collection: 'users' })

module.exports = mongoose.model('users', userSchema)
