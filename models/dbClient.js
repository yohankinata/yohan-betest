// modules
const mongoose = require('mongoose');

// CONNECTION EVENTS
// If the connection throws an error
mongoose.connection.on('error', (error) => {
  console.error(`Error in MongoDb connection: ${error}`)
})

// When successfully connected
mongoose.connection.on('connected', () => {
  console.log(`MongoDB connected!`)
})

// When connection on connecting
mongoose.connection.on('connecting', () => {
  console.log(`MongoDB connecting!`)
})

// When successfully reconnected
mongoose.connection.on('reconnected', () => {
  console.log(`MongoDB reconnected!`)
})

// If the connection get timeout
mongoose.connection.on('timeout', () => {
  console.log(`MongoDB connection timeout!`)
})

// When the connection is disconnected
mongoose.connection.on('disconnected', () => {
  console.log(`MongoDB disconnected!`)
})

module.exports = mongoose
