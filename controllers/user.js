//import services
const { validateInput } = require('../services/commonService');
const userService = require('../services/userService');

// Create
exports.create = async (req, res) => {
    const body = req.body;

    // Validate request
    const validate = validateInput(body,{
        userName : 'string',
        accountNumber : 'number',
        emailAddress : 'string',
        identityNumber : 'number',
    });
    if(!validate.status) {
        return res.status(400).json(validate);
    }
    
    try {

        const user = await userService.create(body);

        return res.status(200).json({
            status: true,
            message: 'success create user',
            data: user,
        });
    } catch (err) {
        console.log(err);
        return res.status(err.code ? err.code : 500).send({
            status: false,
            message: err.message
        });
    }
};

// Retrieve all User from the database.
exports.findAll = async (req, res) => {
    try {
        const users = await userService.getAll();

        return res.status(200).json({
            status:true,
            message: 'success get all users',
            data: users,
        });
    }catch (err) {
        return res.status(err.code ? err.code : 500).send({
            status: false,
            message: err.message
        });
    }
};

// Find a single user with params
exports.findOne = async (req, res) => {
    const getBy = req.params.getBy;
    const value = req.params.value;

    try {
        const user = await userService.getByParams(getBy,value);

        if(!user){
            return res.status(400).send({
                status: false,
                message: 'user not found!'
            });
        }
        return res.status(200).send({
            status: true,
            message: `success get user by ${getBy}`,
            data:user,
        });

    }catch (err) {
        return res.status(err.code ? err.code : 500).send({
            status: false,
            message: err.message
        });
    }
};

// Update a user by the id in the request
exports.update = async (req, res) => {
    const id = req.params.id;
    const body = req.body;

    // Validate request
    const validate = validateInput(body,{
        userName : 'string',
        accountNumber : 'number',
        emailAddress : 'string',
        identityNumber : 'number',
    });
    if(!validate.status) {
        return res.status(400).json(validate);
    }

    try {

        const user = await userService.update(id,body);

        return res.status(200).send({
            status: true,
            message: `success update user`,
            data:user,
        });

    }catch (err) {
        return res.status(err.code ? err.code : 500).send({
            status: false,
            message: err.message
        });
    }
};

// Delete a user with the specified id in the request
exports.delete = async (req, res) => {
    const id = req.params.id;

    try {
        const user = await userService.delete(id);

        await kafkaService.producer(user);

        return res.status(200).send({
            status: true,
            message: `success delete user`,
            data:user,
        });

    }catch (err) {
        return res.status(err.code ? err.code : 500).send({
            status: false,
            message: err.message
        });
    }
};