require('dotenv').config();

module.exports = {
  auth: {
      username: process.env.AUTH_USERNAME,
      password : process.env.AUTH_PASSWORD,
  },
  jwt: {
    secret: process.env.JWT_SECRET,
    session: false,
    expiresIn: 86400,
    issuer: 'YOHAN',
    audience: 'USRA',
    algorithm: 'HS256',
  },
  redis: {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
    password: process.env.REDIS_PASSWORD,
  },
  mongoDB: {
    connectionString: process.env.MONGO_CONNECTION,
    autoIndex: true,
  },
};
