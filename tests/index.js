const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../index');

require('dotenv').config();

const {
    auth: { username, password}
} = require('../config/config');

chai.use(chaiHttp);
chai.should();

describe("Users", () => {
    describe("Testing User", () => {
        it("should get users", (done) => {
            chai.request(app)
                .get('/')
                .end((err, res) => {
                    res.should.have.status(200);

                    chai.request(app)
                        .post('/api/login')
                        .send({
                            username,
                            password,
                        })
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.data.should.have.property('token');
                            var token = res.body.data.token;

                            chai.request(app)
                                .get('/api/user')
                                .set('Authorization', 'JWT ' + token)
                                .end((err,res)=>{
                                res.should.have.status(200);
                                res.body.should.be.a('object');
                                done();
                            });
                        });
                });
        });
        it("should get user by params", (done) => {
            chai.request(app)
                .get('/')
                .end((err, res) => {
                    res.should.have.status(200);

                    chai.request(app)
                        .post('/api/login')
                        .send({
                            username,
                            password,
                        })
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.data.should.have.property('token');
                            var token = res.body.data.token;

                            const getBy = 'accountNumber';
                            const value = 1234;

                            chai.request(app)
                                .get(`/api/user/${getBy}/${value}`)
                                .set('Authorization', 'JWT ' + token)
                                .end((err,res)=>{
                                    res.should.have.status(200);
                                    res.body.should.be.a('object');
                                    done();
                                });
                        });
                });
        });
    });
});